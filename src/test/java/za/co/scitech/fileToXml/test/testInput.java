package za.co.scitech.fileToXml.test;

import org.junit.Assert;
import org.junit.Test;
import za.co.scitech.fileToXml.file.TxtFileReader;
import za.co.scitech.fileToXml.processor.XmlFormatter;

/**
 * Created with IntelliJ IDEA.
 * User: Marc
 * Date: 2014/05/26
 * Time: 5:01 PM
 * To change this template use File | Settings | File Templates.
 */
public class testInput {

    @Test
    public void runTest(){
        TxtFileReader fileReader = new TxtFileReader("input.txt");
        XmlFormatter formatter = new XmlFormatter(fileReader);
        try {
            formatter.marshall();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            Assert.fail(e.getMessage());
        }
    }
}
