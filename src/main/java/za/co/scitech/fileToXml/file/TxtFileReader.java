package za.co.scitech.fileToXml.file;

import za.co.scitech.fileToXml.model.List;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * Created with IntelliJ IDEA.
 * User: Marc
 * Date: 2014/05/26
 * Time: 4:28 PM
 *
 */
public class TxtFileReader implements XmlReader {

    private String fileName;

    //Caution SimpleDateFormat is not thread safe!!!!
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public TxtFileReader(String fileName){
        this.fileName = fileName;
    }

    @Override
    public List process() throws Exception{
        InputStream is = this.getClass().getResourceAsStream("/"+fileName);
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        List list = new List();

        while((line = br.readLine()) != null){
            try{
               list.getItem().add(convertString(line));
            } catch (Exception e){
                //Ignoring Line - Does not match format
            }
        }
        return list;
    }

    private List.Item convertString(String s) throws Exception{
        StringTokenizer tokenizer = new StringTokenizer(s," ",true);

        StringBuffer dateTime = new StringBuffer();
        //TODO: I really dont like this approach - There has to be a better way? Maybe string.split?
        dateTime.append(tokenizer.nextToken());  //Get Date
        dateTime.append(tokenizer.nextToken());  //Next should be space
        dateTime.append(tokenizer.nextToken());  //Get Time
        try{
            tokenizer.nextToken();    //Next Space
        }catch (NoSuchElementException e){

        }

        //Finally, all the rest onto the message.
        StringBuilder sb = new StringBuilder();
        while(tokenizer.hasMoreTokens()){
            sb.append(tokenizer.nextToken()) ;
        }


        List.Item item = new List.Item();
        Date dateValue = dateFormat.parse(dateTime.toString());
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(dateValue);
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);

        item.setDate(xmlGregorianCalendar);
        item.setLabel(sb.toString());
        return item;

    }


}
