package za.co.scitech.fileToXml.file;

import za.co.scitech.fileToXml.model.List;

/**
 * Created with IntelliJ IDEA.
 * User: Marc
 * Date: 2014/05/26
 * Time: 4:54 PM
 */
public interface XmlReader {
    List process() throws Exception;
}
