package za.co.scitech.fileToXml.processor;

import za.co.scitech.fileToXml.file.XmlReader;
import za.co.scitech.fileToXml.model.List;
import za.co.scitech.fileToXml.model.ObjectFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * Created with IntelliJ IDEA.
 * User: Marc
 * Date: 2014/05/26
 * Time: 4:52 PM
 *
 */
public class XmlFormatter {

    private ObjectFactory of;
    XmlReader reader;

     public XmlFormatter(XmlReader reader){
         this.reader = reader;
    }

    public void marshall() throws Exception{
        List list = reader.process();
        marshal(list);
    }

    private void marshal(List list) throws Exception{
            JAXBContext jc = JAXBContext.newInstance( "za.co.scitech.fileToXml.model" );
            Marshaller m = jc.createMarshaller();
            m.marshal( list, System.out );
    }
}
